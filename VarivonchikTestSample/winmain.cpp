#undef UNICODE
#define IDC_SEDIT 1001 
#define IDC_MEDIT 1006
#define IDC_COLORBUTTON 1010
#define INPUTDATADIALOG 101
#include <windows.h>
#include  <math.h>
#include <cmath>
#include <string>
#include <vector>
#include <algorithm>
#include <iterator>
#include <sstream>
#include <numeric>
using namespace std;

struct Stud
{
	string surname;
	vector<int> marks;
	double average;
	COLORREF color;
};

vector<Stud> _students;
double GetAverage(const vector<int>& marks);
void  ChooseCl(HWND& hwnd, COLORREF& src);
BOOL InitApplication(HINSTANCE hinstance);
BOOL InitInstance(HINSTANCE hinstance, int nCmdShow);
LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam);

int WINAPI WinMain(HINSTANCE hinstance, HINSTANCE prevHinstance, LPSTR lpCmdLine, int nCmdShow)
{
	MSG msg;

	if (!InitApplication(hinstance))
	{
		MessageBox(NULL, "Unable to Init App", "Error", MB_OK);
		return FALSE;
	}

	if (!InitInstance(hinstance, nCmdShow))
	{
		MessageBox(NULL, "Unable to Init Instance", "Error", MB_OK);
		return FALSE;
	}

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg.wParam;
}

BOOL InitApplication(HINSTANCE hinstance)
{
	WNDCLASSEX wndclass;
	wndclass.cbSize = sizeof(wndclass);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc = WndProc;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hinstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_CROSS);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = "Graph";
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	if (!RegisterClassEx(&wndclass))
	{
		MessageBox(NULL, "Cannot register class", "Error", MB_OK);
		return FALSE;
	}
	return TRUE;
}
BOOL CALLBACK InputProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam)
{
	static COLORREF colorsSelected[4];
	static HWND surnamesHWND[4], marksHWND[4], buttons[4];
	static vector<Stud> res;
	switch (message)
	{
	case WM_INITDIALOG:
	{
		string text;
		for (int i = 0; i < 4; i++)
		{
			surnamesHWND[i] = GetDlgItem(hwnd, IDC_SEDIT + i);
			text = "Surname" + to_string(i + 1);
			SetWindowText(surnamesHWND[i], text.data());
			marksHWND[i] = GetDlgItem(hwnd, IDC_MEDIT + i);
			buttons[i] = GetDlgItem(hwnd, IDC_COLORBUTTON + i);
			res.push_back(Stud());
			res[i].color = RGB(255, 255, 255);
		}
	}
	break;
	case WM_COMMAND:
		switch (LOWORD(wparam))
		{
		case IDOK:
			char buff[256];
			for (int i = 0; i < 4; i++)
			{
				GetWindowText(surnamesHWND[i], buff, 255);
				res[i].surname = string(buff);
				GetWindowText(marksHWND[i], buff, 255);
				string data; data = string(buff);
				istringstream iss(data);
				for (int j = 0; j < 3; j++)
				{
					string mark;
					iss >> mark;
					res[i].marks.push_back(atoi(mark.data()));
				}
				res[i].average = GetAverage(res[i].marks);
			}
			_students = res;
			res = vector<Stud>();
			EndDialog(hwnd, 0);
			break;
		case IDCANCEL:
			res = vector<Stud>();
			EndDialog(hwnd, -1);
			break;
		case IDC_COLORBUTTON:
			ChooseCl(hwnd, res[0].color);
			break;
		case IDC_COLORBUTTON + 1:
			ChooseCl(hwnd, res[1].color);
			break;
		case IDC_COLORBUTTON + 2:
			ChooseCl(hwnd, res[2].color);
			break;
		case IDC_COLORBUTTON + 3:
			ChooseCl(hwnd, res[3].color);
			break;
		}
	}
	return FALSE;
}
LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam)
{
	static double x, y;
	static HDC hdc;
	static int code;
	PAINTSTRUCT ps;
	switch (message)
	{
	case WM_CREATE:
		code = DialogBox(GetModuleHandle(NULL), MAKEINTRESOURCE(INPUTDATADIALOG), hwnd, InputProc);
		break;
	case WM_SIZE:
		x = LOWORD(lparam);
		y = HIWORD(lparam);
		break;
	case WM_PAINT:
	{
		hdc = BeginPaint(hwnd, &ps);
		vector<POINT> points;
		double columnWidth = x / _students.size();
		for (int i = 0; i < _students.size();i++)
		{
			POINT pt;
			pt.x = i * columnWidth; pt.y = y - (y * (_students[i].average  / 10.0 ));
			MoveToEx(hdc, pt.x, y, NULL);
			LineTo(hdc, pt.x,pt.y);
			points.push_back(pt);
		}
		
		SetPixel(hdc, points[0].x, points[0].y, BLACK_PEN);
		for(int i =0;i<points.size() - 2;i++)
		{
			MoveToEx(hdc, points[i].x, points[i].y, NULL);
			LineTo(hdc, points[i + 1].x, points[i + 1].y);
		}
		LineTo(hdc, points[points.size() - 1].x, points[points.size() - 1].y);
		SetPixel(hdc, points[points.size() - 1].x, points[points.size() - 1].y, BLACK_PEN);
		EndPaint(hwnd, &ps);
		break;
	}

	case WM_LBUTTONDOWN:
	{
		break;
	}

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hwnd, message, wparam, lparam);
	}
	return FALSE;
}
BOOL InitInstance(HINSTANCE hinstance, int nCmdShow)
{
	HWND hwnd;
	hwnd = CreateWindow(
		"Graph",
		"Graph",
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		0,
		CW_USEDEFAULT,
		0,
		NULL,
		NULL,
		hinstance,
		NULL);


	if (!hwnd)
		return FALSE;
	ShowWindow(hwnd, nCmdShow);
	UpdateWindow(hwnd);
	return TRUE;
}

double GetAverage(const vector<int>& marks)
{
	return ((double)accumulate(marks.begin(), marks.end(), 0)) / marks.size();
}

void ChooseCl(HWND& hwnd, COLORREF& src)
{
	CHOOSECOLOR ccs;
	COLORREF acrCustClr[16];
	ccs.lStructSize = sizeof(CHOOSECOLOR);
	ccs.hwndOwner = hwnd;
	ccs.rgbResult = RGB(255, 255, 255);
	ccs.Flags = CC_RGBINIT | CC_FULLOPEN;
	ccs.lpCustColors = (LPDWORD)acrCustClr;
	ccs.rgbResult = src;
	if (ChooseColor(&ccs))
	{
		src = ccs.rgbResult;
	}
}